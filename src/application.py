import apache_beam as beam
from beam_nuggets.io import kafkaio
from decouple import config
from services.sonarcloud.transform import SonarCloudTransform
import logging

logging.basicConfig(level=logging.INFO)


consumer_config = {"topic": config('TOPIC'),
                   "bootstrap_servers": config('SERVERS'),
                   "group_id": config('GROUP_ID')}


with beam.Pipeline(runner='DirectRunner') as pipeline:


    credential = (pipeline|  "Reading messages from Kafka"  >> kafkaio.KafkaConsume(
                                        consumer_config=consumer_config))

    credential | "Extract Data from SonarCloud and Write Database" >> SonarCloudTransform()

    


    