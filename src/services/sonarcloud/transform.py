from apache_beam.transforms import PTransform
import apache_beam as beam
from .fn import FnExtract
from services.repository.mongodbio import WriteToMongoDB
from decouple import config

class SonarCloudTransform(PTransform):
    def __init__(self):
        self.database = "sonarcloud"
        self.entities = ['project']

    def expand(self, pcoll):
        result = None
        for entity in self.entities:
            result = (
                pcoll
                | "Retrieve {}".format(entity) >>beam.ParDo(FnExtract(function_name=entity)) 
                | "Save {} in Database".format(entity) >> WriteToMongoDB (uri=config('MONGO_URL'), db= self.database, coll=entity, batch_size=2)
            )
        return result
