import apache_beam as beam
from .extract import Extract
import json

class FnExtract(beam.DoFn):
   
   """ Abstract Function"""
   def __init__(self,function_name) -> None:
      super().__init__()
      self.application_entity = Extract()
      self.function_name = function_name
      
      
   def process(self, element):
      
      data = json.loads(element[1])
         
      self.application_entity.config (
         entity = self.function_name,
         personal_access_token = data['secret'],
         sonar_url = data['sonar_url'],
         organization_uuid = data['configuration_uuid'],
         configuration_uuid = data['organization_uuid'],
         organization_name = data['organization_name']
         )

      return self.application_entity.do()
            
      
      