import logging
import json
import datetime
from sonarcloudx1 import factories

logging.basicConfig(level=logging.INFO)

class Extract():
    """Abstract Class with the main function used to extract data application and save in a MongoDB"""

    def __init__(self):

        self.instance = None
        
    def config (self, entity, personal_access_token, sonar_url, organization_uuid,configuration_uuid, organization_name) :

        

        self.personal_access_token = personal_access_token
        self.sonar_url = sonar_url
        self.organization_uuid = organization_uuid
        self.configuration_uuid = configuration_uuid
        self.organization_name = organization_name
        
        print ("{}-{}-{}".format( self.sonar_url, self.personal_access_token,self.organization_name))

        self.extract = {
            
            'project': factories.ProjectFactory(personal_access_token=self.personal_access_token,
                                                        sonar_url=self.sonar_url)    
            }

        self.instance = self.extract[entity]
        self.instance.set_organization_name(organization_name)

    def do(self):
        """Main function to retrieve and save data in a MongoDB's collection
        
        Args:
            dict data: credentials (secret, url etc) to connect a application
        
        """
        try:
            logging.info("Start Retrieve Information")
            
            data_extracted = self.instance.get_all(today=False)  
            
            logging.info("End Returning")
            
            return data_extracted
            
        except Exception as e: 
            logging.error("OS error: {0}".format(e))
            logging.error(e.__dict__)    


    def __json_default(self,value):
        """ Convert a specific object to dict 
        
        Args:
            object value: a object to convert to dict
        """
        if isinstance(value, datetime.date):
            return dict(year=value.year, month=value.month, day=value.day)
        else:
            return value.__dict__

    def object_to_dict(self, entity):
        """ Convert an object to dict 
        
        Args:
            object entity: a entity to convert to dict
        """
        json_entity = json.dumps(entity,ensure_ascii=False,default = lambda o: self.__json_default(o), sort_keys=True, indent=4).encode('utf-8')
        return json.loads(json_entity)
    