FROM apache/beam_python3.10_sdk:latest

WORKDIR /src

COPY src/requirements.txt requirements.txt
RUN pip install --no-cache-dir -r requirements.txt

COPY . .

CMD [ "run.sh"]


